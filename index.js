import express from 'express';
import fetch from 'isomorphic-fetch';
import Promise from 'bluebird';
import _ from 'lodash';

const __DEV__ = true;

const app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.get('/task2A', (req, res, next) => {
  let a = +req.query.a;
  let b = +req.query.b;
  if (Number.isNaN(a)) a = 0;
  if (Number.isNaN(b)) b = 0;
  const c = a + b;
  return res.json(c);
});

app.listen(3000, () => {
  console.log('Example app listening on port 3000!');
});
